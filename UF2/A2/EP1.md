Processament de continguts
==========================

MP9UF2A2EP1

Exercici pràctic (EP1)

Característiques de l’exercici
------------------------------

Exercici pràctic.

### Tipus d’exercici

Aquest exercici servirà per mesurar les habilitats en utilitzar MarkDown
i Git, i es realitzarà en combinació a l’exercici pràctic final de la
unitat formativa.

### Criteri de qualificació

L’exercici aporta el 100% de la nota del resultat d’aprenentatge
associat a l’activitat.
